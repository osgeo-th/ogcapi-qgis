Saraphi Get Connect
===
![Icon](./img/osgeo.svg)
Saraphi Get Connect เป็นปลั๊กอินสำหรับสื่อสารกับข้อมูบบนเซิร์ฟเวอร์ มีฟังก์ชันในการสร้างข้อมูล เรียกดูข้อมูล แก้ไขข้อมูล และลบข้อมูลบนคลาวด์ได้อย่างสะดวกผ่านมาตรฐาน OGC API 


วิธีใช้งาน
--

**1. ล็อกอิน**

  - หากยังไม่ได้สมัครใช้งาน กด Register เพื่อสมัครใช้งาน
  - ใส่ Email และ Password เพื่อเข้าสู่ระบบ

    ![Icon](./img/readme/logins.PNG)


**2. สร้าง Collection**
  - กดปุ่ม Import เพื่อสร้าง Collection ใหม่ หรือ เพิ่อมข้อมูลใน Collection ที่มีอยู่แล้ว
  - เลือกข้อมูลที่ต้องการเพิ่มเข้าไปใน Collection ในช่อง Select layers (สามารถเลือกได้มากว่า 1 ชั้นข้อมูล)
  - หากต้องการเพิ่มเฉพาะข้อมูลที่เลือกอยู่ เลือก  Only selected features
  - กดปุ่ม Refresh เพื่อโหลดข้อมูลใน Layer panel เข้ามาใน Select layers 
  - เลือก Import to new Collection เพื่อสร้าง Collection ใหม่แล้วเพิ่มข้อมูลที่เลือกเข้าไปใน Collection แล้วกดปุ่ม Create
  - เลือก Import to Collection เพื่อเพิ่มข้อมูลที่เลือกเข้าไปใน Collection ที่มีอยู่แล้วโดยเลือก Collection name ที่ต้องการเเพิ่มข้อมูล แล้วกดปุ่ม Create

      ![Icon](./img/readme/collection_list_import.png)

      ![Icon](./img/readme/create_collection.PNG)


**3. ดึงข้อมูลใน Collection**
  - เลือก Collection ที่ต้องการดึงข้อมูลเขามาใน QGIS 
  - กด Load เพื่อดึงข้อมูลเขามาใน QGIS 

    ![Icon](./img/readme/collection_list_load.png)

    **ผลลัพธ์**

    ![Icon](./img/readme/items.PNG)


**4. แก้ไข้ข้อมูลใน แก้ไข้ข้อมูลใน Collection**
  - เมื่อมีแก้ไข้ข้อมูลของ Collection ที่ดึงลงมาแล้ว
  - เลือก Collection ที่ต้องการเเก้ไข้ข้อมูลบนคลาวด์ในช่อง Collection
  - กดปุ่ม Update เพื่อเเก้ไข้ข้อมูลบนคลาวด์

    ![Icon](./img/readme/collection_list_update.png)

**5. ลบ Collection**
  - เลือก Collection ที่ต้องการลบในช่อง Collection
  - กดปุ่ม Delete 

    ![Icon](./img/readme/collection_list_delete.png)

**6. ใช้งาน Thailand BaseMap**
  - กด Load ในช่อง BaseMaps เพื่อใช้งาน Thailand BaseMap

    ![Icon](./img/readme/collection_list_baseMap.png)  

    **ผลลัพธ์**

    ![Icon](./img/readme/Thailand_BaseMap.PNG)  

**7. รายละเอียดอื่นๆ** 
  - กดปุ่ม Help เพื่อดูรายระเอียดอื่นๆเกี่ยวกับ OSGeo-TH 

    ![Icon](./img/readme/collection_list_help.png) 

**8. ออกจากระบบ**
  - กด Sigh Out เพื่ออกจากระบบ

    ![Icon](./img/readme/collection_list_sighout.png) 
