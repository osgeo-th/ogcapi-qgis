import requests
import os
from shutil import copyfile
import sqlite3
from urllib.request import urlopen

os.chdir(os.path.dirname(__file__))
path = os.getcwd()

def create_floder(dirName):
    if not os.path.exists(dirName):
        os.makedirs(dirName)
        #print("Directory " , dirName ,  " Created ")
        return dirName
    else:    
        #print("Directory " , dirName ,  " already exists")   
        return dirName   


def copy_vector_file(source_file):
    destination_file = source_file.split('vector')[0] + "/" + "vector_copy"
    destination_file = create_floder(destination_file)
    destination_file = destination_file + "/" + source_file.split('/')[-1].split(".geojson")[0] + "_copy.geojson"

    
    # #print(source_file)

    copyfile(source_file,destination_file)


def checkNetConnection():
    try:
        urlopen('http://www.google.com', timeout=10)
        return True
    except Exception as err:
        pass
    return False


def write_js_file(tiles,tile_style,bounds):
    os.chdir(os.path.dirname(__file__))
    file_path = os.getcwd().split("utils")[0] + "preview/tile.js"
    #print(file_path)
    with open(file_path, 'w') as file:
        file.write("var data_source = {'vectortile':'"+ tiles + "','vectortile_style':'" + tile_style +"','bounds':"+ str(bounds) + "}")
    
    file = os.getcwd().split("utils")[0] +"/preview/index.html"
    return file

def Sort(sub_li): 

    return(sorted(sub_li, key = lambda x: x[0])) 
