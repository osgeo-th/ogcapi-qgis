
import sqlite3


class DB:
    def __init__(self,path_file,db_name):
        self.path_db = path_file + "/"+db_name+".sqlite"
        #print(self.path_db)

    def create_db(self,table_name,colum_list=[]):

    # colum_list = [  "id integer PRIMARY KEY AUTOINCREMENT","server text","email text","password text"]    #data test

        sql_cmd =  'CREATE TABLE '+table_name+' ('
        for d in colum_list:
            sql_cmd = sql_cmd + d
            if d != colum_list[-1]:
                sql_cmd = sql_cmd+','
            else:
                sql_cmd = sql_cmd+')'

        #print(sql_cmd)
        with sqlite3.connect(self.path_db) as con:
            con.execute(sql_cmd)   


    def insert(self,table_name,data=[]) :

        # data = [  ["server","vallaris.co.th"],["email","anon.555@gmail.com"],["password","44444"]]    # data test

        sql_cmd =  'INSERT INTO '
        table = table_name + '('
        values =' values('
        for d in data:
            table = table + d[0]
            values = values + '"'+d[1]
            if d != data[-1]:
                table = table+','
                values = values+'",'
            else:
                table = table+')'
                values = values+'")'
        sql_cmd = sql_cmd + table + values

        with sqlite3.connect(self.path_db) as con:
            #print(sql_cmd)
            con.execute(sql_cmd)  

    def insert_multi(self,table_name,data=[]) : 

        sql_cmd =  'INSERT INTO '+table_name
        text = ""
        for d in data[0]:
            text  = text+"?"
            if d != data[0][-1]:
                text  = text+","
        values =' values(' + text + ');'   
        sql_cmd = sql_cmd + values
        #print(values)

        with sqlite3.connect(self.path_db) as con:  
            c = con.cursor()
            #insert multiple records in a single query
            c.executemany(sql_cmd,data);              


    def update(self,table_name,data=[],id=""):

        # data =[["email","anonjjj@gmail.com"]["pass","2259859"]]
        sql_cmd = "UPDATE "+ table_name + " SET" 
        value =""
        condition = "WHERE id = "+str(id)
        for d in data:
            value = d[0]+" = "+ d[1]
            if d != data[-1]:
                value = value+','  
        sql_cmd = sql_cmd + value + condition
                  


        # UPDATE table_name
        # SET 
        # column1 = value1, column2 = value2...., columnN = valueN
        # WHERE [condition];        


    def select(self,sql_cmd = "") :

        if sql_cmd != "":
            with sqlite3.connect(self.path_db) as con:
                res = con.execute(sql_cmd)    
                return res

    def delete_from_table(self,table_name=''):
        with sqlite3.connect(self.path_db) as con:
            sql_cmd = "delete from "+table_name

            con.execute(sql_cmd)                          


