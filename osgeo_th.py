# -*- coding: utf-8 -*-
"""
/***************************************************************************
 OSGeo_Plugins
                            A QGIS plugin
 Plugin for using the OGC API from for CRUD gis data
                              -------------------
        begin                : 2018-07-18
        by                   : i-bitz company limited
        copyright            : (C) 2020 by https://osgeo.in.th
        email                : foss4gthailand@gmail.com

 ***************************************************************************/
"""
from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .osgeo_th_dialog import OSGeo_PluginsDialog
import os.path,subprocess
import webbrowser

from SaraphiGetConnect.osgeo_module.osgeo_core import *



class OSGeo_Plugins:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):

        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'OSGeo_Plugins_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&OSGeo-TH')




        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('OSGeo_Plugins', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):

        icon_path = ':/plugins/SaraphiGetConnect/img/osgeo.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Saraphi Get Connect'),
            callback=self.run,
            parent=self.iface.mainWindow())

        # will be set False in run()
        self.first_start = True

        self.dlg = OSGeo_PluginsDialog()


    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Saraphi Get Connect'),
                action)
            self.iface.removeToolBarIcon(action)


    def run(self):
        """Run method that performs all the real work"""

        # Create the dialog with elements (after translation) and keep reference
        # Only create GUI ONCE in callback, so that it will only load when the plugin is started
        if self.first_start == True:
            self.first_start = False

            url = "https://openapi.osgeo.in.th"
            
            self.dlg.setMaximumSize(420, 250) 
            self.dlg.setMinimumSize(420, 250) 

            self.osgeo_core = osgeo_core(self.dlg,self.iface,url)
            self.dlg.login.setDefault(False)


            ###layout
            self.dlg.login.clicked.connect(self.osgeo_core.logins)
            self.dlg.logout.clicked.connect(self.osgeo_core.logout) 
            self.dlg.back.clicked.connect(self.osgeo_core.back)  
            self.dlg.create_new.clicked.connect(self.osgeo_core.create_new_collection_layout) 

            ## function
            self.dlg.create.clicked.connect(self.osgeo_core.create_new_collection)
            self.dlg.delete_collaction.clicked.connect(self.osgeo_core.delete_collection)
            self.dlg.add_items.clicked.connect(self.osgeo_core.get_items) 
            self.dlg.update.clicked.connect(self.osgeo_core.update_collection_items) 
            self.dlg.add_maps.clicked.connect(self.osgeo_core.get_vectortile) 
            self.dlg.help.clicked.connect(self.osgeo_core.open_webbrowser) 
            self.dlg.refresh.clicked.connect(self.osgeo_core.add_layer_to_list) 
            self.dlg.old_collection.toggled.connect(self.osgeo_core.import_selection)
            self.dlg.new_collection.toggled.connect(self.osgeo_core.import_selection)

            self.dlg.password.returnPressed.connect(self.osgeo_core.logins)
            self.dlg.email.returnPressed.connect(self.osgeo_core.logins)
            self.dlg.collection_name.returnPressed.connect(self.osgeo_core.create_new_collection)

        # show the dialog
            self.dlg.show()
        else:
            self.dlg.close()
            self.dlg.show()

        # Run the dialog event loop
        # result = self.dlg.exec_()
        # See if OK was pressed
        # if result:
        #     # Do something useful here - delete the line containing pass and
        #     # substitute with your code.  
        #     pass







     
                    
