
from SaraphiGetConnect.osgeo_module.osgeo_api import *
from SaraphiGetConnect.DB.db import *
import os.path,tempfile,base64,shutil,json,time,hashlib,webbrowser
from SaraphiGetConnect.utils.utils import *
from qgis.PyQt import QtWidgets
from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication,QUrl,Qt
from qgis.PyQt.QtGui import QIcon,QColor
from qgis.PyQt.QtWidgets import QAction,QLineEdit,QMessageBox
from qgis.PyQt.QtWebKit import QWebSettings
from qgis.PyQt.QtWebKitWidgets import QWebView, QWebInspector, QWebPage
from SaraphiGetConnect.convert_style import *
from PyQt5 import QtCore
from qgis.core import *
from qgis.utils import iface,Qgis

# import qgis.utils
class osgeo_core:
    def __init__(self,dlg,iface,url=""):

        self.dlg =dlg
        self.iface = iface
        self.url= url
        self.osgeo_api = osgeo_api(self.url)
        self.temp_path = tempfile.gettempdir() 
        self.osgeo_temp_path = self.temp_path+"//osgeo"
        self.message_box = QMessageBox()
        self.type_list = ["Polygon","LineString","Point","MultiPolygon","MultiLineString","MultiPoint"]  
        # ,"MultiPolygon","MultiLineString","MultiPoint"

    def logins(self):

        if checkNetConnection() == True:
    
            email = self.dlg.email.text().strip()
            password = self.dlg.password.text().strip()
            password_hex = base64.b64encode(password.encode("utf-8"))
            token = self.osgeo_api.logins(email,password)

            if token != "error":
                self.dlg.layout_login.hide()
                self.dlg.layout_collection.show()                
                self.get_collection_list()
                self.dlg.setMinimumSize(460, 420)  
                self.dlg.setMaximumSize(460, 420) 
                self.dlg.layout_collection.move(25, 15)   
                title ="Saraphi Get Connect"
                self.dlg.setWindowTitle(title)   
                

            else:
                message="Incorrect email or password." 
                self.message_box.warning(self.iface.mainWindow(), 'Warning', message)                    

            
        else:
            message="No internet connection." 
            self.iface.messageBar().pushMessage("Warning  ", message, level=1, duration=3)            


    def logout(self):
    
        self.dlg.layout_collection.hide()   
        self.dlg.collection_list.clear() 
        self.dlg.collection_list2.clear()   
        self.dlg.password.setText("") 
        self.dlg.email.setText("")    
        self.dlg.new_collection.setChecked(True)
        self.dlg.layout_login.show()          
        self.dlg.setMaximumSize(420, 250) 
        self.dlg.setMinimumSize(420, 250) 
        self.dlg.resize(420, 250) 

        title ="Saraphi Get Connect"
        self.dlg.setWindowTitle(title)           

        try:
            # os.rmdir(self.osgeo_temp_path)
            shutil.rmtree(self.osgeo_temp_path, ignore_errors=True)
        except:
            pass     
           

    def create_new_collection_layout(self):
    
        self.dlg.setMinimumSize(395, 355)  
        self.dlg.setMaximumSize(395, 355)         
        self.dlg.layout_login.hide()
        self.dlg.layout_collection.hide()         
        self.dlg.layout_new_collection.move(25, 15)           
        self.dlg.layout_new_collection.show() 
        title = "Create new collection"
        self.dlg.setWindowTitle(title)  
        self.add_layer_to_list() 

    def add_layer_to_list(self):
    
        self.dlg.layer_list.clear()
        all_layer=QgsProject.instance().mapLayers().values() 
        vlayer = []
        self.vlayer_id = []

        for i in all_layer:
            if i.type() == QgsMapLayer.VectorLayer and i.name() != "background":
                vlayer.append(i.name())
                self.vlayer_id.append(i.id())
        
        self.dlg.layer_list.addItems(vlayer)         


    def  get_collection_list(self):
    
        self.collection_list = self.osgeo_api.get_collection_list()

        if self.collection_list != False:
            self.dlg.collection_list.clear()
            self.dlg.collection_list2.clear()
            if len(self.collection_list) != 0:
                collection_name_list = [i["title"] for i in self.collection_list]
                self.dlg.collection_list.addItems(collection_name_list)  
                self.dlg.collection_list2.addItems(collection_name_list)    

            self.collection_index = self.dlg.collection_list.currentRow()
        else:
            self.dlg.collection_list.clear()
            self.dlg.collection_list2.clear()            
            self.dlg.collection_list.addItems([]) 
            self.dlg.collection_list2.addItems([]) 



    def create_new_collection(self):
        self.init_progressBar()
        if checkNetConnection() == True:
        
            temp_path = self.temp_path+"/osgeo"
            create_floder(temp_path)
            selected_layer_list = self.dlg.layer_list.selectedItems()

            if len(selected_layer_list)>0:

                self.progress_changed(0)                
                self.featureCount = True

                for d in selected_layer_list:
                    registry = QgsProject.instance()
                    name = d.text()
                    layer = registry.mapLayersByName(name)[0]
                    iface.setActiveLayer(layer) 
                    vlayer = iface.activeLayer()
                    if vlayer.featureCount() == 0:
                        self.featureCount = False
                    else:
                        continue
                    
                self.progress_changed(10) 
                                
                collection_name = self.dlg.collection_name.text()

                self.progress_changed(20) 
                if self.featureCount == True:  
                    if  self.dlg.new_collection.isChecked() :                                
                        if   collection_name != "":
                            # if  self.dlg.old_collection.isChecked() :                          
                                collection_json = {
                                    "title" : collection_name,
                                    "description" : collection_name,
                                    "itemType" : "Feature",
                                    "links" : []  
                                }

                                collection_id = self.osgeo_api.create_collection(collection_json)
                        else:  
                            if   collection_name == "":
                                message="collection name is required"
                                self.message_box.warning(self.iface.mainWindow(), 'Warning',
                                                        message)      
                                self.progress_changed(100)                                 
                    else:
                        
                        collection_id = self.collection_list[self.dlg.collection_list2.currentIndex ()]["id"]

                    self.progress_changed(25) 
                    add_progress = 30//len(selected_layer_list)
                    for d in selected_layer_list:
                        self.progress.setValue(self.progress.value()+add_progress)
                        registry = QgsProject.instance()
                        name = d.text()
                        layer = registry.mapLayersByName(name)[0]
                        iface.setActiveLayer(layer) 
                        vlayer = iface.activeLayer()   
                        vlayer_json = temp_path+'/'+vlayer.name()+'.geojson'

                        exp_crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
                        if self.dlg.selected_features.isChecked():                   
                            writer = QgsVectorFileWriter.writeAsVectorFormat(vlayer,vlayer_json,'UTF-8',exp_crs,'GeoJson',True)   
                        else:   
                            writer = QgsVectorFileWriter.writeAsVectorFormat(vlayer,vlayer_json,'UTF-8',exp_crs,'GeoJson',False)      
                        data={}
                        with open(vlayer_json, encoding='UTF-8') as f:
                            data = json.load(f)   
                            # data = {key:val for key, val in data.items() if key != 'name' and key != 'crs'} 
                        self.progress_changed(60) 
                        
                        st = 0
                        end =500
                        for i in range((len(data['features'])//500)+1):
                            features_format = {"type": "FeatureCollection","features": []}
                            try:
                                features_format["features"] = data['features'][st:end]                          
                                status = self.osgeo_api.create_items(collection_id,features_format) 
                                st += 500
                                end += 500     

                            except Exception as e:
                                # print("ERROR",e)
                                break                     

                    self.progress_changed(85) 

                    # if status == True:
                    self.progress_changed(90) 
                    self.dlg.collection_name.setText("")
                    self.get_collection_list()
                    self.progress_changed(100) 
                    self.back()

                                     
                else:
                    message="no features in selected Layer"
                    self.message_box.warning(self.iface.mainWindow(), 'Warning',
                                            message)  
                    self.progress_changed(100) 
            else:
                message="selected Layer"
                self.message_box.warning(self.iface.mainWindow(), 'Warning',
                                        message)  
                self.progress_changed(100)           
        else:
            message="No internet connection." 
            self.iface.messageBar().pushMessage("Warning  ", message, level=1, duration=3)                


    def delete_collection(self):

        if checkNetConnection() == True:

            if self.dlg.collection_list.count() != 0 :
            
                message= "You are sure you want to delete this collection."
                self.message_box.setIcon(QMessageBox.Warning)
                self.message_box.setText(message)
                self.message_box.setWindowTitle("delete collection")
                self.message_box.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
                returnValue = self.message_box.exec()
                collection_id = str(self.collection_list[self.dlg.collection_list.currentRow()]["id"] )

                if returnValue == QMessageBox.Ok:

                    self.osgeo_api.delete_collection(collection_id)
                    self.get_collection_list()
                    # self.back()   
            else:
                message="Don't have the collection."
                self.message_box.warning(self.iface.mainWindow(), 'Warning',
                                        message)                       

        else:            
            message="No internet connection." 
            self.iface.messageBar().pushMessage("Warning  ", message, level=1, duration=3) 


    def get_items(self):
    
        if checkNetConnection() == True:

            self.init_progressBar()
            self.progress_changed(0)
            features_list = []  

            if self.dlg.collection_list.count() != 0 :
                self.collection_id = str(self.collection_list[self.dlg.collection_list.currentRow()]["id"] )
                self.progress_changed(5)
                ly_path = self.osgeo_api.collection_items(self.collection_id,progress =self.progress)
                self.progress_changed(20)
                if ly_path != False:
                
                    geom_list = self.check_geom(ly_path)                                    
                    self.progress_changed(25)
                                
                    ly_output_path= ly_path.split(ly_path.split("/")[-1])[0]
                    ##create group
                    root = QgsProject.instance().layerTreeRoot()
                    if root.findGroup(self.collection_list[self.dlg.collection_list.currentRow()]["title"]) == None:
                        shapeGroup = root.addGroup(self.collection_list[self.dlg.collection_list.currentRow()]["title"])
                        # cloned group
                        cloned_group1 = shapeGroup.clone()
                        # move the node (along with sub-groups and layers) to the top
                        root.insertChildNode(0, cloned_group1) 
                        # remove the original node
                        root.removeChildNode(shapeGroup)                
                    
                    shapeGroup = root.findGroup(self.collection_list[self.dlg.collection_list.currentRow()]["title"])
                    if shapeGroup.findGroup("Vector") == None:            
                        myGroupA = shapeGroup.addGroup("Vector")     
                    
                    GpName =  self.osgeo_temp_path+'/'+self.collection_id+'/vector/'+self.collection_id+'.gpkg'
                    self.progress_changed(30)
                    num = 0
                    for i,type in enumerate(self.type_list) :
                        
                        if type in geom_list :
                            ly = QgsVectorLayer(ly_path +'|geometrytype='+type, type, "ogr")
                        
                            if num == 0:
                                firstt = True
                                options = QgsVectorFileWriter.SaveVectorOptions()                        
                                if firstt :            
                                    firstt = False
                                else :
                                    options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer 
                                    options.EditionCapability = QgsVectorFileWriter.CanAddNewLayer  
                                options.layerName = self.collection_list[self.dlg.collection_list.currentRow()]["title"] +"_"+ type
                                _writer = QgsVectorFileWriter.writeAsVectorFormat(ly, GpName, options )    
                                num+=1            
                            else:
                                options = QgsVectorFileWriter.SaveVectorOptions()
                                options.actionOnExistingFile = QgsVectorFileWriter.CreateOrOverwriteLayer 
                                options.layerName = self.collection_list[self.dlg.collection_list.currentRow()]["title"] +"_"+ type
                                _writer = QgsVectorFileWriter.writeAsVectorFormat(ly,GpName, options)

                            gpkg_countries_layer = GpName + "|layername="+self.collection_list[self.dlg.collection_list.currentRow()]["title"] +"_"+ type
                            vlayer = QgsVectorLayer(gpkg_countries_layer, self.collection_list[self.dlg.collection_list.currentRow()]["title"]+"_"+ type, "ogr")                

                            if not vlayer.isValid():
                                print("Layer failed to load!")
                            else:

                                QgsProject.instance().addMapLayer(vlayer) 

                                myLayer = root.findLayer(vlayer.id())
                                myClone = myLayer.clone()
                                parent = myLayer.parent()

                                myGroup = shapeGroup.findGroup("Vector")
                                # Insert in first position
                                myGroup.insertChildNode(0, myClone)

                                parent.removeChildNode(myLayer)  

                                vl = QgsProject.instance().mapLayersByName(self.collection_list[self.dlg.collection_list.currentRow()]["title"]+"_"+ type)[0]
                                self.iface.setActiveLayer(vl)   

                                layer = self.iface.activeLayer()
                                features = layer.getFeatures()

                                for feat in features:                            
                                    features_data = str(feat.attributes()) + str(feat.geometry())
                                    hash_object = hashlib.md5(features_data.encode('utf-8'))
                                    # if str(feat['_id']) =="NULL" :
                                    #             id = "new"                                 
                                    features_list.append([str(feat['_id']),str(hash_object.hexdigest())])
                        else:
                            pass
                        self.progress.setValue(self.progress.value()+15) 
                        
                        self.iface.zoomToActiveLayer()


                    self.progress_changed(80)
                    sql_file_path  =    self.osgeo_temp_path +"/"+self.collection_id
                    self.osgeo_data_db = DB( sql_file_path,"osgeo_data")   
                    
                    #create DataOld tale
                    tb_name ="DataOld"
                    try:
                        colum_list = ["id text", "features text"]
                        self.osgeo_data_db.create_db(tb_name,colum_list)                
                    except:
                        pass

                    # delete value in table
                    self.osgeo_data_db.delete_from_table(tb_name)
                    self.osgeo_data_db.insert_multi(tb_name,features_list)
                    self.progress_changed(100)

                else:
                    self.progress_changed(100)
                    if ly_path == False:  
                        message = "The area you are trying to download does not have a items."
                        self.message_box.warning(self.iface.mainWindow(), 'Warning',
                                                message)                    
            else:
                message = "Don't have the collection."
                self.message_box.warning(self.iface.mainWindow(), 'Warning',
                                        message)                    

        else:
            message="Don't have collection." 
            self.iface.messageBar().pushMessage("Warning  ", message, level=1, duration=3)                 



    def check_geom(self,file_path):
        
        with open(file_path) as f:
            data = json.load(f)
        features_type_list = set([i['geometry']['type'] for i in data['features']])     
        
        return features_type_list  


    def update_collection_items(self):
        collection_id = str(self.collection_list[self.dlg.collection_list.currentRow()]["id"] )        
        start_time = time.time()

            
        if checkNetConnection() == True:
            self.init_progressBar()
            self.progress_changed(0)            

            edit_status = False
            collection_name = self.collection_list[self.dlg.collection_list.currentRow()]["title"]    
            sql_file_path  =    self.osgeo_temp_path +"/"+collection_id+"/vallaris.sqlite"              
            features_list = []

            items_count = 0
            i = 0
            for type in  self.type_list :   
                self.progress.setValue(self.progress.value()+5)     

                vl = QgsProject.instance().mapLayersByName(collection_name+"_"+ type)
                if len(vl) == 0:
                    continue
                else:
                    self.iface.setActiveLayer(vl[0])   

                    layer = self.iface.activeLayer()
                    feature_count = layer.featureCount()
                    items_count += feature_count
                    features = layer.getFeatures()
                    for feat in features: 
                        if str(feat['_id']) =="NULL" :
         
                            id = "new" 
                        else:
                            id =   str(feat['_id'])   
                            
                        features_data = str(feat.attributes()) + str(feat.geometry())
                        hash_object = hashlib.md5(features_data.encode('utf-8'))
                        features_list.append((id,str(hash_object.hexdigest())))

                    #create DataOld tale
                    tb_name ="DataNew"
                    try:
                        colum_list = ["id text", "features text"]
                        self.osgeo_data_db.create_db(tb_name,colum_list)                
                    except:
                        pass
                    
                    if i == 0:
                    # delete value in table
                        self.osgeo_data_db.delete_from_table(tb_name)

                    # self.osgeo_data_db.insert(tb_name,features_list)
                    if len(features_list) != 0:

                        self.osgeo_data_db.insert_multi(tb_name,features_list) 
                        # layer_update = True
                    edit_status = True
                    i+=1
 
            if edit_status == True and items_count > 0:
                self.progress_changed(20)    

                add = self.osgeo_data_db.select("SELECT DataNew.id FROM DataNew WHERE id NOT IN (SELECT id FROM DataOld )")
                edit = self.osgeo_data_db.select("SELECT A.id FROM DataOld A, DataNew B WHERE A.id = B.id AND A.features != B.features")
                delete = self.osgeo_data_db.select("SELECT DataOld.id FROM DataOld WHERE id NOT IN (SELECT id FROM DataNew )")

                add_id = [i[0] for i in add]
                edit_id = [i[0] for i in edit]
                delete_id = [i[0] for i in delete]
                item_id=[add_id, edit_id, delete_id]

                if len(item_id[0]) != 0 or len(item_id[1]) != 0 or len(item_id[2]) != 0 :
                    delete_id = item_id[2]

                    for i,d in enumerate(item_id):
                        self.progress.setValue(self.progress.value()+5)
                        if i == 0:
                            add_str = ""                
                            if  "new" in d:
                                if len(set(d))>1:
                                    add_str = '"_id" IS NULL or '   
                                if len(set(d))==1:
                                    add_str = '"_id" IS NULL '                                        
                            for i in list(dict.fromkeys(d)):
                                if i != "new":                
                                    add_str += '"_id"='
                                    add_str += "'"+i+"'"
                                    if len(d)>1 and i != list(dict.fromkeys(d))[-1] :
                                        add_str += " or "

                        if i == 1:
                            edit_str =""
                            for i in list(dict.fromkeys(d)):
                                if len(d) > 0:
                                    edit_str += '"_id"='
                                    edit_str += "'"+i+"'"
                                if len(d)>1 and i != list(dict.fromkeys(d))[-1]:
                                    edit_str += " or "                                               

                    if len(delete_id) != 0:
    
                        for id in delete_id:                                     
                            self.osgeo_api.delete_items(collection_id, id)            

                    add_items = {"type": "FeatureCollection","features": []}                        
   

                    for i in self.type_list:
                        self.progress.setValue(self.progress.value()+5)
                        vl = QgsProject.instance().mapLayersByName(collection_name+"_"+ i)

                        if len(vl) != 0 :
                            self.iface.setActiveLayer(vl[0])   
                            layer = self.iface.activeLayer()  
                
                            if add_str != "" and edit_str != "":
                                layer.selectByExpression(str(edit_str )+ ' or ' + str(add_str) )
                            elif edit_str == "":
                                layer.selectByExpression(str(add_str ))
                            else:
                                layer.selectByExpression(str(edit_str ))
                            if layer.selectedFeatureCount() > 0:
                                
                                fields = layer.dataProvider().fields() 
                                field_index = [i for i,d in enumerate( fields) if i != 0 ]

                                osgeo_update_items_path = self.osgeo_temp_path +"/"+collection_id+"/update_items"   
                                create_floder(osgeo_update_items_path)              
                                layer_json = osgeo_update_items_path+'/'+layer.name()+'.geojson'

                                exp_crs = QgsCoordinateReferenceSystem(4326, QgsCoordinateReferenceSystem.EpsgCrsId)
                                writer = QgsVectorFileWriter.writeAsVectorFormat(layer,layer_json,'UTF-8',exp_crs,'GeoJson',attributes=field_index, onlySelected=True )     

                                data = {}
                                with open(layer_json) as f:
                                    data = json.load(f)

                                for d in data['features']:
                                    id = d['properties']['_id']

                                    if id  in  edit_id:  
                                        self.osgeo_api.update_items(collection_id, id,d) 
                                    else:                                              
                                        add_items["features"].append(d)     
                                    
                    # add item list
                    data = add_items 
                    st = 0
                    end =500
                    if len(add_items['features']) > 0:
                        for i in range((len(data['features'])//500)+1):
                            features_format = {"type": "FeatureCollection","features": []}
                            try:
                                features_format["features"] = data['features'][st:end]                          
                                status = self.osgeo_api.create_items(collection_id,features_format) 
    
                                st += 500
                                end += 500       
                            except Exception as e:
                                # print("ERROR",e)
                                break                            

                    for type in self.type_list :
                        vl = QgsProject.instance().mapLayersByName(collection_name+"_"+ type)  
                        if len(vl) != 0  :  
                            self.iface.setActiveLayer(vl[0])   
                            layer = self.iface.activeLayer()   
                            QgsProject.instance().removeMapLayers([layer.id()])     
                    self.progress_changed(100)
                    message="Get items." 
                    self.iface.messageBar().pushMessage("information  ", message, level=0, duration=3)
                    self.get_items()   
                            
                else:
                    self.progress_changed(100)
                    message="No item was edited."
                    self.message_box.information(self.iface.mainWindow(), 'Information',
                                            message)     
                          

            elif edit_status == True and items_count == 0:
                
                delete = self.osgeo_data_db.select("SELECT DataOld.id FROM DataOld WHERE id NOT IN (SELECT id FROM DataNew )")
                delete_id = [i[0] for i in delete]
                
                if len(delete_id) != 0:
                    value = len(delete_id)//60
                    for id in delete_id:    
                        self.progress.setValue(self.progress.value()+value)                                 
                        self.osgeo_api.delete_items(collection_id, id)    
                self.progress_changed(100)               
            
            else:   
                self.progress_changed(100) 
                message="No collection items in layers panel."
                self.message_box.information(self.iface.mainWindow(), 'Information',
                                        message)  
                                                
        
        else:
            self.progress_changed(100)
            message="No internet connection." 
            self.iface.messageBar().pushMessage("Warning  ", message, level=1, duration=3)
              


    def get_vectortile(self):

        QGIS_version = Qgis.QGIS_VERSION_INT

        if QGIS_version >= 31400:
            collection_name = self.dlg.map_list.currentText()

            url = "https://maps.osgeo.in.th/data/v3/{z}/{x}/{y}.pbf"

            root = QgsProject.instance().layerTreeRoot()
            if root.findGroup("BaseMap") == None:
                shapeGroup = root.addGroup("BaseMap")

                cloned_group1 = shapeGroup.clone()
                # move the node (along with sub-groups and layers) to the top
                root.insertChildNode(0, cloned_group1) 
                # remove the original node
                root.removeChildNode(shapeGroup)               

            ds = QgsDataSourceUri() 
            ds.setParam("type","xyz")
            ds.setParam("zmin","0")
            ds.setParam("zmax","24")
            ds.setParam("url", url)
            uri = bytes(ds.encodedUri()).decode('utf-8')

            vtlayer = QgsVectorTileLayer(uri, collection_name)
            QgsProject.instance().addMapLayer(vtlayer)  
                        
            layer = QgsProject.instance().mapLayersByName(collection_name)[0]
            root = QgsProject.instance().layerTreeRoot()

            myLayer = root.findLayer(vtlayer.id())
            myClone = myLayer.clone()
            parent = myLayer.parent()

            myGroup = root.findGroup("BaseMap")
            # Insert in first position
            myGroup.insertChildNode(0, myClone)
            parent.removeChildNode(myLayer)
            layer_1 = QgsProject.instance().mapLayersByName( collection_name )[0]
            self.iface.setActiveLayer(layer_1)

            style = self.osgeo_api.vectortile_style()
            renderer, labeling = parse_json(style)

            self.iface.activeLayer().setRenderer(renderer)
            self.iface.activeLayer().setLabeling(labeling)    
        else:
            message="QGIS version does not support vector tiles. QGIS Minimum Version 3.14" 
            self.iface.messageBar().pushMessage("Warning  ", message, level=1, duration=3)            


    def init_progressBar(self, min_value=0, max_value=100):
        """
         Initializes the progress bar within with the given min and max value.

        :param min_value: (integer)
        :param max_value: (integer)
        """

        # clear the message bar
        self.iface.messageBar().clearWidgets()
        # set a new message bar
        self.progressMessageBar = self.iface.messageBar()
        self.progress = QtWidgets.QProgressBar()
        self.progress.setAlignment(QtCore.Qt.AlignCenter)
        self.progress.setStyleSheet(" QProgressBar::chunk "
                                "{"                                
                                "background: #467F85;" 
                                "QProgressBar { text-align: center; }"                              
                                "}" 
                                "QProgressBar"
                                "{"
                                "color: rgb(255, 255, 255);"
                                "background-color : lightblue;"
                                "border : 1px"
                                "}"                                
                                )                                        

        # Maximum is set to 100, making it easy to work with percentage of completion
        # If minimum and maximum both are set to 0, the bar shows a busy indicator instead of a percentage of steps.
        self.progress.setMinimum(min_value)
        self.progress.setMaximum(max_value)

        # pass the progress bar to the message Bar
        self.progressMessageBar.pushWidget(self.progress, duration=5)

    def progress_changed(self,progress_num):
        self.progress.setValue(progress_num) 
        if progress_num == 100:
            self.progressMessageBar.clearWidgets()


    def open_webbrowser(self):
    		webbrowser.open('https://osgeo.in.th/')            


    def import_selection(self):
        if self.dlg.new_collection.isChecked() == True:
            self.dlg.old_collection.setChecked(False)
            self.dlg.label_old.setDisabled(True)
            self.dlg.label_new.setDisabled(False)
            self.dlg.collection_list2.setDisabled(True)
            self.dlg.collection_name.setDisabled(False)
        else:   
            self.dlg.old_collection.setChecked(True)
            self.dlg.label_old.setDisabled(False)
            self.dlg.label_new.setDisabled(True)
            self.dlg.collection_list2.setDisabled(False) 
            self.dlg.collection_name.setDisabled(True)                    

    def back(self):
        
        self.dlg.layout_collection.show()
        self.dlg.layout_new_collection.hide()   
        self.dlg.setMinimumSize(460, 420) 
        self.dlg.setMaximumSize(460, 420) 
        title = "Saraphi Get Connect"
        self.dlg.setWindowTitle(title)  
        self.dlg.collection_name.setText("")  
        self.dlg.selected_features.setChecked(False)
        self.dlg.new_collection.setChecked(True)
        self.dlg.new_collection.setChecked(False)