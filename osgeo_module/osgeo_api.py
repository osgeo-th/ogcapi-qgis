import requests
import tempfile
from SaraphiGetConnect.utils.utils import *

# from vallaris_connectors.utils.utils import *
class osgeo_api:

    def __init__(self,url=''):
        self.url = url
        self.token = ''
        self.temp_path = tempfile.gettempdir()


    def logins(self,email,password):

        url = self.url +"/1.0/login"
        payload = '{"email":"'+email+'","password":"'+password+'"}'
        headers = {
        'Content-Type': 'application/json'
        }

        response = requests.post( url, headers=headers, data = payload)

        if response.status_code == 200:
            response = response.json()            
            token = response["accessToken"]
            self.token = token
            return token
        else:
            return "error"   


    def logins1(self,email,password):

        url = self.url +"/v1.0/login"
        payload = '{"username":"'+email+'","password":"'+password+'"}'
        headers = {
        'Content-Type': 'application/json'
        }
        response = requests.post( url, headers=headers, data = payload)

        if response.status_code == 200:
            response = response.json()            
            token = response["token"]
            self.token = token
            return token
        else:
            return "error"   


    def get_collection_list(self):

        url = self.url + "/1.0/collections"

        payload = {}
        headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + self.token
        }
        response = requests.get( url, headers=headers, data = payload)

        if response.status_code == 200:    
            response = response.json()["collections"]
            return response
        else:
            return False 


    def create_collection(self,collection_json):
    
        url = self.url + "/1.0-beta/collections"
        collection_json = str(collection_json)
        collection_json = collection_json.replace("integer", "int")
        collection_json = collection_json.replace("'", '"')
        payload= collection_json 

        headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+self.token
        }

        response = requests.post( url, headers=headers, data=payload.encode('utf-8'))

        if response.status_code == 201:
            response = response.json()
            collection_id = response['id']
            return collection_id  
        else:
            # print(response.text)
            return False




    def collection_items(self,collection_id,limit=None,progress = None):
        
        osgeo_path = self.temp_path+"/osgeo"
        collection_vector_path =  create_floder(osgeo_path+"/"+collection_id+"/vector")
        collection_vector_file_path = collection_vector_path + "/"+collection_id+".geojson"                    
        page = 1
        self.break_ = True
        json_write = {}

        while True:    

            if self.break_ == False:
                break
            else:
                url = self.url + "/1.0/collections/"+collection_id+"/items?token="+self.token +"&page="+str(page)+"&limit=10000" 
                payload={}
                headers = {
                    'Content-Type': 'application/json',                    
                    'Authorization': 'Bearer '+self.token
                }

                response = requests.get(url, headers=headers, data=payload)   
                if response.status_code == 200:                 
                    if page == 1:
                        json_write = response.json()
                        page +=1    
                    else: 
                        json_write['features'].extend(response.json()['features']) 
                        page +=1  
                elif response.status_code == 404 and page == 1:
                    self.break_ = False 

                else:    
            
                    for i in json_write['features'] :                                            
                        del i['id']
                        # i.pop('id', None)           

                    with open(collection_vector_file_path, 'w') as f:
                        f.write(str(json_write).replace("'", '"').replace("None", "null") ) 
                    self.break_ = False       

        if page > 1:
            return collection_vector_file_path      
        else:
            return False         


    def delete_collection(self,collection_id):

        url = self.url + "/1.0-beta/collections/"+collection_id
        payload={}
        headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+self.token        
        }

        response = requests.delete( url, headers=headers, data=payload)
                   

    def create_items(self,collection_id,items):

        url = self.url + "/1.0-beta/collections/"+collection_id+"/items"        
        payload= str(items).replace("'fid'", "'fid_'").replace("'", '"').replace("None", "null")

        headers = {
        'Content-Type': 'application/json',            
            'Authorization': 'Bearer '+self.token    
        }

        response = requests.post(url, headers=headers, data=payload.encode('utf-8'))

        if response.status_code == 201 :
            return True
        else:
            print(response.text)
            return False   


    def update_items(self,collection_id,items_id,items):
        
        url = self.url + "/1.0-beta/collections/"+collection_id+"/items/"+items_id
        items = str(items)        
        items = items.replace("None", "''")
        items = str(items).replace("'", '"')
        payload= items

        headers = {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+self.token
        }

        response = requests.put(url, headers=headers, data=payload.encode('utf-8'))



    def delete_items(self,collection_id,items_id):

        url = self.url + "/1.0-beta/collections/"+collection_id+"/items/"+items_id
        payload={}
        headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer '+self.token        
        }

        response = requests.delete( url, headers=headers, data=payload)
        # print(response.text)
    

    def vectortile_style(self):
        url = "https://maps.osgeo.in.th/styles/thailand_basemap_1.0/style.json"

        payload = {}
        headers = {}
        response = requests.get( url, headers=headers, data = payload)
        response = response.json()

        return response        